import { render } from '/web_modules/preact.js';
import { html } from '/web_modules/htm/preact.js';
import Router from '/web_modules/preact-router.js';
import { createStore, Provider, connect } from '/web_modules/unistore/full/preact.es.js'

import Top from '/src/pages/Top.js';
import Article from '/src/pages/Article.js';

const store = createStore({
    menuLinksProps: [
        {
            child: 'top',
            href: '/'
        },
        {
            child: 'life',
            href: '/article/life'
        },
        {
            child: 'game',
            href: '/article/game'
        }
    ],
    article: {
        date: {
            y: '2020',
            m: '2',
            d: '27'
        },
        text: 'あいう\n弱肉強食'
    }
});

const Main = () => html`
<${Provider} store=${store}>
    <${Router}>
        <${Top} path="/" />
        <${Article} path="/article/:articleId" />
    </${Router}>
</${Provider}>
`;

render(html`<${Main}/>`, document.getElementById('app'));