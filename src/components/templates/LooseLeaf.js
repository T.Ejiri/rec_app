import { h } from '/web_modules/preact.js';
import { useMemo } from '/web_modules/preact/hooks.js';
import { html } from '/web_modules/htm/preact.js';
import picostyle from '/web_modules/picostyle.js';

const ps = picostyle(h);

const Frame = ps('div')({
    position: 'absolute',
    // top: '0',
    // bottom: '0',
    // right: '0',
    // left: '0',
    margin: 'auto',
    width: '90vw',
    height: '90vh'
});

const SheetColor = '#FFFAEE';
const SheetHeight = '500px';

const Sheet = ps('div')({
    position: 'absolute',
    left: '50px',
    zIndex: '1',
    width: '80vw',
    height: SheetHeight,
    backgroundColor: SheetColor,
    borderRadius: '0 10px 10px 0',
    padding: '60px 30px'
});

const DummySheet = ps('div')({
    position: 'absolute',
    height: SheetHeight
});


const Hole = ps('div')({
    width: '50px',
    height: '50px',
    background: `radial-gradient(circle closest-side at center, transparent 50%, ${SheetColor} 50%)`
});

const LineColor = '#AAAAAA';
const FontColor = '#333333';

const DateLine = ps('div')({
    position: 'relative',
    borderBottom: `solid ${LineColor} 1px`,
    fontSize: '10px',
    width: '150px'
});

const DateFont = ps('span')({
    color: FontColor,
    fontSize: '15px'
});

const TopLine = ps('div')({
    borderBottom: `solid ${LineColor} 2px`,
    marginTop: '30px'
});

const Line = ps('p')({
    fontSize: '20px',
    verticalAlign: 'bottom',
    borderBottom: `solid ${LineColor} 1px`,
    marginTop: '10px',
    letterSpacing: '2px',
    color: FontColor
});

// const ThinLine = ps('div')({
//     borderBottom: 'solid #AAAAAA 1px'
// });

/*
interface Props {
    date?: {
        y?: string | number;
        m?: string | number;
        d?: string | number;
    };
    // 表示するテキスト(改行したい場合は`\n`を用いる)
    text?: string;
}
*/

const LooseLeaf = props => {
    const date = useMemo(() => {
        if (props.date === void 0) return { y: '    ', m: '  ', d: '  ' };
        
        const y = props.date.y || '';
        const m = props.date.m || '';
        const d = props.date.d || '';
        
        return { y, m, d };
    }, [props.date]);
    
    const lines = props.text ? props.text.split('\n') : [];
    
    return html`
<${Frame}>
    <${DummySheet} class="hole">
        <${Hole} />
        <${Hole} />
    </${DummySheet}>
    <${Sheet} class="sheet">
        <${DateLine}>
            Date: <${DateFont}>${date.y}.${date.m}.${date.d}</${DateFont}>
        </${DateLine}>
        <${TopLine} />
        ${lines.map(line => html`<${Line}>${line}</${Line}>`)}
    </${Sheet}>
</${Frame}>
`;
};



export default LooseLeaf;