import { html } from '/web_modules/htm/preact.js';
import { connect } from '/web_modules/unistore/full/preact.es.js'

const Menu = connect(['menuLinksProps'], () => {})(
props => {
    console.info(props.menuLinksProps);
return html`
    <nav>
        Menu
        ${props.menuLinksProps && props.menuLinksProps.map(menuLinkProps => {
            const { child, ...rest } = menuLinkProps;
            return html`
                <a ...${rest}>${child}</a>
            `;
        })}
    </nav>`
});

export default Menu;