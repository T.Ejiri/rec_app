import { html } from '/web_modules/htm/preact.js';
import { connect } from '/web_modules/unistore/full/preact.es.js'

import Menu from '/src/components/templates/Menu.js'
import LooseLeaf from '/src/components/templates/LooseLeaf.js'

const Article = connect(['article'], () => {})(props => html`
<${LooseLeaf} text=${props.article.text} date=${props.article.date}>
    Article(${props.articleId})
</${LooseLeaf}>
<${Menu} />
`);

export default Article;