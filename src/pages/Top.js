import { html } from '/web_modules/htm/preact.js';

import Menu from '/src/components/templates/Menu.js'

const Top = props => html`
<div>
    TOP
    <${Menu} />
</div>`;

export default Top;